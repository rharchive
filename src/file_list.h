/*
 *  rharchive
 *
 *  The Radio Helsinki Archive Daemon
 *
 *
 *  Copyright (C) 2009-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rharchive.
 *
 *  rharchive is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rharchive is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rharchive. If not, see <http://www.gnu.org/licenses/>.
 *
 *  In addition, as a special exception, the copyright holders hereby
 *  grant permission for non-GPL-compatible GStreamer plugins to be used
 *  and distributed together with GStreamer and rharchive.
 *  This permission goes above and beyond the permissions granted by the
 *  GPL license rharchive is covered by.
 */

#ifndef RHARCHIVE_file_list_h_INCLUDED
#define RHARCHIVE_file_list_h_INCLUDED

#include <sys/types.h>
#include <time.h>

#include <glib.h>

#include "slist.h"
#include "sysexec.h"

#define FILE_CLOSED -1
#define FILE_POST_PROCESS -2

struct file_struct {
  int fd_;
  char* path_;
  mode_t mode_;
  int nocache_;
  child_t* pp_child_;
};
typedef struct file_struct file_t;

struct file_list_struct {
  slist_t list_;
  GMutex mutex_;
};
typedef struct file_list_struct file_list_t;

int file_list_init(file_list_t* list);
void file_list_clear(file_list_t* list);
file_t* file_list_add(file_list_t* list, struct tm* time, const char* type, const char* format, const char* dir, mode_t mode, int nocache);
int file_list_remove(file_list_t* list, int fd);
int file_list_call_post_process(file_list_t* list, int fd, char* script);
int file_list_waitpid(file_list_t* list);
int open_file(file_t* file);

#endif
