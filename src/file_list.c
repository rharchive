/*
 *  rharchive
 *
 *  The Radio Helsinki Archive Daemon
 *
 *
 *  Copyright (C) 2009-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rharchive.
 *
 *  rharchive is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rharchive is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rharchive. If not, see <http://www.gnu.org/licenses/>.
 *
 *  In addition, as a special exception, the copyright holders hereby
 *  grant permission for non-GPL-compatible GStreamer plugins to be used
 *  and distributed together with GStreamer and rharchive.
 *  This permission goes above and beyond the permissions granted by the
 *  GPL license rharchive is covered by.
 */

#define _GNU_SOURCE
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <glib.h>

#include "datatypes.h"
#include "file_list.h"
#include "slist.h"
#include "log.h"

static void delete_file(void* element)
{
  file_t* deletee = (file_t*)element;
  log_printf(INFO, "removing/closing file '%s' -> %d", deletee->path_, deletee->fd_);
  if(deletee->path_) free(deletee->path_);
  if(deletee->fd_ >= 0) close(deletee->fd_);
  if(deletee->pp_child_) free_child(deletee->pp_child_);
}

int file_list_init(file_list_t* list)
{
  g_mutex_init(&(list->mutex_));
  return slist_init(&(list->list_), &delete_file);
}

void file_list_clear(file_list_t* list)
{
  g_mutex_lock(&(list->mutex_));
  slist_clear(&(list->list_));
  g_mutex_unlock(&(list->mutex_));
}

file_t* file_list_add(file_list_t* list, struct tm* time, const char* type, const char* format, const char* dir, mode_t mode, int nocache)
{
  if(!list || !(&(list->mutex_)))
    return NULL;

  file_t* tmp = malloc(sizeof(file_t));
  if(!tmp)
    return NULL;

  log_printf(INFO, "%s time: %02d:%02d:%02d on %d.%d.%d%s", type, time->tm_hour, time->tm_min, time->tm_sec, time->tm_mday, time->tm_mon+1, time->tm_year+1900, time->tm_isdst > 0 ? " (DST)": "");

  char name[256];
  strftime(name, sizeof(name), format, time);
  int len = asprintf(&(tmp->path_), "%s/%s", dir, name);
  if(len == -1) {
    free(tmp);
    return NULL;
  }

  log_printf(INFO, "%s filename is: %s(.?)", type, tmp->path_);
  tmp->fd_ = FILE_CLOSED;
  tmp->mode_ = mode;
  tmp->nocache_ = nocache;
  tmp->pp_child_ = NULL;

  g_mutex_lock(&(list->mutex_));
  if(slist_add(&(list->list_), tmp) == NULL) {
    g_mutex_unlock(&(list->mutex_));
    free(tmp->path_);
    free(tmp);
    return NULL;
  }
  g_mutex_unlock(&(list->mutex_));

  return tmp;
}

int file_list_remove(file_list_t* list, int fd)
{
  if(!list || !(&(list->mutex_)))
    return -1;

  g_mutex_lock(&(list->mutex_));
  slist_element_t* tmp = list->list_.first_;
  while(tmp) {
    if(((file_t*)tmp->data_)->fd_ == fd) {
      slist_remove(&(list->list_), tmp->data_);
      break;
    }
    tmp = tmp->next_;
  }
  g_mutex_unlock(&(list->mutex_));

  return 0;
}

int file_list_call_post_process(file_list_t* list, int fd, char* script)
{
  if(!list || !(&(list->mutex_)))
    return -1;

  g_mutex_lock(&(list->mutex_));
  slist_element_t* tmp = list->list_.first_;
  while(tmp) {
    if(((file_t*)tmp->data_)->fd_ == fd) {
      log_printf(INFO, "calling post processing for '%s'", ((file_t*)tmp->data_)->path_);
      close(((file_t*)tmp->data_)->fd_);
      ((file_t*)tmp->data_)->fd_ = FILE_POST_PROCESS;

      char* const argv[] = { script, ((file_t*)tmp->data_)->path_, NULL };
      char* const evp[] = { NULL };
      ((file_t*)tmp->data_)->pp_child_ = rh_exec(script, argv, evp);
      if(!((file_t*)tmp->data_)->pp_child_)
        slist_remove(&(list->list_), tmp->data_);

      break;
    }
    tmp = tmp->next_;
  }
  g_mutex_unlock(&(list->mutex_));

  return 0;
}

int file_list_waitpid(file_list_t* list)
{
  if(!list || !(&(list->mutex_)))
    return -1;

  g_mutex_lock(&(list->mutex_));
  slist_element_t* tmp = list->list_.first_;
  while(tmp) {
    if(((file_t*)tmp->data_)->fd_ == FILE_POST_PROCESS) {
      int ret = rh_waitpid(((file_t*)tmp->data_)->pp_child_, NULL);
      file_t* deletee = tmp->data_;
      tmp = tmp->next_;
      if(ret)
        slist_remove(&(list->list_), deletee);
    }
    else
      tmp = tmp->next_;
  }
  g_mutex_unlock(&(list->mutex_));

  return 0;
}

int open_file(file_t* file)
{
  if(!file || file->fd_ != FILE_CLOSED) // file already open!
    return -1;

  char* orig_path = file->path_;
  int cnt = 0;
  do {
    int flags = O_WRONLY | O_CREAT | O_EXCL;
    if(file->nocache_)
      flags |= O_DIRECT;
    file->fd_ = open(file->path_, flags, file->mode_);
    if(file->fd_ < 0) {
      if(errno != EEXIST) {
            // TODO: thread safe strerror
        log_printf(ERROR, "can't open file '%s': %s", file->path_, strerror(errno));
        if(orig_path != file->path_)
          free(orig_path);
        file->fd_ = FILE_CLOSED;
        return -1;
      }
      cnt++;
      char* tmp;
      int len = asprintf(&tmp, "%s.%d", orig_path, cnt);
      if(len == -1) {
        if(orig_path != file->path_)
          free(orig_path);
        return -2;
      }

      if(file->path_ != orig_path)
        free(file->path_);
      file->path_ = tmp;
    }
    fchmod(file->fd_, file->mode_);
  }
  while(file->fd_ < 0);

  if(orig_path != file->path_)
    free(orig_path);

  log_printf(INFO, "opened file '%s' -> %d", file->path_, file->fd_);

  return 0;
}
