/*
 *  rharchive
 *
 *  The Radio Helsinki Archive Daemon
 *
 *
 *  Copyright (C) 2009-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rharchive.
 *
 *  rharchive is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rharchive is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rharchive. If not, see <http://www.gnu.org/licenses/>.
 *
 *  In addition, as a special exception, the copyright holders hereby
 *  grant permission for non-GPL-compatible GStreamer plugins to be used
 *  and distributed together with GStreamer and rharchive.
 *  This permission goes above and beyond the permissions granted by the
 *  GPL license rharchive is covered by.
 */

#include "datatypes.h"

#include "log.h"

#include "sig_handler.h"

#include <glib.h>
#include <errno.h>

#include "rhmain.h"

static GThread *signal_thread;

void signal_init()
{
  sigset_t signal_set;

  sigemptyset(&signal_set);
  sigaddset(&signal_set, SIGINT);
  sigaddset(&signal_set, SIGQUIT);
  sigaddset(&signal_set, SIGHUP);
  sigaddset(&signal_set, SIGTERM);
  sigaddset(&signal_set, SIGUSR1);
  sigaddset(&signal_set, SIGUSR2);

  pthread_sigmask(SIG_BLOCK, &signal_set, NULL);
}

static gpointer signal_thread_func(gpointer data)
{
  RHMainLoop *loop = (RHMainLoop *)data;

  struct timespec timeout;
  sigset_t signal_set;
  int sig_num;
  for(;;) {
    sigemptyset(&signal_set);
    sigaddset(&signal_set, SIGINT);
    sigaddset(&signal_set, SIGQUIT);
    sigaddset(&signal_set, SIGHUP);
    sigaddset(&signal_set, SIGTERM);
    sigaddset(&signal_set, SIGUSR1);
    sigaddset(&signal_set, SIGUSR2);
    timeout.tv_sec = 1;
    timeout.tv_nsec = 0;
    sig_num = sigtimedwait(&signal_set, NULL, &timeout);
    if(sig_num == -1) {
      if(errno != EINTR && errno != EAGAIN) {
        log_printf(ERROR, "sigwait failed with error: %d, signal handling will be disabled", errno);
        break;
      }
    } else {
        switch(sig_num) {
        case SIGTERM:
        case SIGINT:
        case SIGQUIT: {
          log_printf(NOTICE, "signal %d received, exiting", sig_num);
          rhmain_loop_quit(loop, sig_num);
          break;
        }
        default: {
          log_printf(NOTICE, "signal %d received, ignoring", sig_num);
          break;
        }
      }
    }
  }

  return NULL;
}

int signal_start(RHMainLoop *loop)
{
  g_assert(!signal_thread);

  signal_thread = g_thread_new("sig_handler", signal_thread_func, loop);
  if(!signal_thread)
    return -1;

  return 0;
}

void signal_stop()
{
  sigset_t signal_set;

  sigemptyset(&signal_set);
  sigaddset(&signal_set, SIGINT);
  sigaddset(&signal_set, SIGQUIT);
  sigaddset(&signal_set, SIGHUP);
  sigaddset(&signal_set, SIGTERM);
  sigaddset(&signal_set, SIGUSR1);
  sigaddset(&signal_set, SIGUSR2);

  pthread_sigmask(SIG_UNBLOCK, &signal_set, NULL);
}
