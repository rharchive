/*
 *  rharchive
 *
 *  The Radio Helsinki Archive Daemon
 *
 *
 *  Copyright (C) 2009-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rharchive.
 *
 *  rharchive is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rharchive is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rharchive. If not, see <http://www.gnu.org/licenses/>.
 *
 *  In addition, as a special exception, the copyright holders hereby
 *  grant permission for non-GPL-compatible GStreamer plugins to be used
 *  and distributed together with GStreamer and rharchive.
 *  This permission goes above and beyond the permissions granted by the
 *  GPL license rharchive is covered by.
 */

#ifndef RHARCHIVE_rhmain_h_INCLUDED
#define RHARCHIVE_rhmain_h_INCLUDED

#include <glib.h>

typedef struct {
  GMainLoop *gloop;
  volatile gint exit_code;
} RHMainLoop;

gboolean rhmain_loop_init(RHMainLoop* loop);
void rhmain_loop_destroy(RHMainLoop* loop);
gint rhmain_loop_run(RHMainLoop* loop);
void rhmain_loop_quit(RHMainLoop* loop, gint exit_code);

#endif
