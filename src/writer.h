/*
 *  rharchive
 *
 *  The Radio Helsinki Archive Daemon
 *
 *
 *  Copyright (C) 2009-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rharchive.
 *
 *  rharchive is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rharchive is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rharchive. If not, see <http://www.gnu.org/licenses/>.
 *
 *  In addition, as a special exception, the copyright holders hereby
 *  grant permission for non-GPL-compatible GStreamer plugins to be used
 *  and distributed together with GStreamer and rharchive.
 *  This permission goes above and beyond the permissions granted by the
 *  GPL license rharchive is covered by.
 */

#ifndef RHARCHIVE_writer_h_INCLUDED
#define RHARCHIVE_writer_h_INCLUDED

#include <gst/gst.h>
#include <glib.h>
#include <time.h>
#include <sys/types.h>

#include "rhmain.h"
#include "file_list.h"

struct writer_struct {
  RHMainLoop *loop_;
  GstElement* sink_;
  GstClock* clock_;
  GstClockID clock_id_;
  GThread* thread_;
  const char* name_format_;
  const char* output_dir_;
  mode_t mode_;
  int nocache_;
  GstClockTime interval_;
  GstClockTime offset_;
  char* post_process_;
  file_list_t files_;
  file_t* current_;
  file_t* next_;
  struct timespec next_boundary_;
};
typedef struct writer_struct writer_t;

int writer_init(writer_t* writer, RHMainLoop *loop, const char* name_format, mode_t mode, int nocache, const char* output_dir, int interval, int offset, char* post_process);
int writer_start(writer_t* writer);
void writer_stop(writer_t* writer);

#endif
