/*
 *  rharchive
 *
 *  The Radio Helsinki Archive Daemon
 *
 *
 *  Copyright (C) 2009-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rharchive.
 *
 *  rharchive is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rharchive is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rharchive. If not, see <http://www.gnu.org/licenses/>.
 *
 *  In addition, as a special exception, the copyright holders hereby
 *  grant permission for non-GPL-compatible GStreamer plugins to be used
 *  and distributed together with GStreamer and rharchive.
 *  This permission goes above and beyond the permissions granted by the
 *  GPL license rharchive is covered by.
 */

#ifndef RHARCHIVE_slist_h_INCLUDED
#define RHARCHIVE_slist_h_INCLUDED

struct slist_element_struct {
  void* data_;
  struct slist_element_struct* next_;
};
typedef struct slist_element_struct slist_element_t;

slist_element_t* slist_get_last(slist_element_t* first);

struct slist_struct {
  void (*delete_element)(void* element);
  slist_element_t* first_;
};
typedef struct slist_struct slist_t;

int slist_init(slist_t* lst, void (*delete_element)(void*));
slist_element_t* slist_add(slist_t* lst, void* data);
void slist_remove(slist_t* lst, void* data);
void slist_clear(slist_t* lst);
int slist_length(slist_t* lst);

#endif
