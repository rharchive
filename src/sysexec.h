/*
 *  rharchive
 *
 *  The Radio Helsinki Archive Daemon
 *
 *
 *  Copyright (C) 2009-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rharchive.
 *
 *  rharchive is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rharchive is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rharchive. If not, see <http://www.gnu.org/licenses/>.
 *
 *  In addition, as a special exception, the copyright holders hereby
 *  grant permission for non-GPL-compatible GStreamer plugins to be used
 *  and distributed together with GStreamer and rharchive.
 *  This permission goes above and beyond the permissions granted by the
 *  GPL license rharchive is covered by.
 */

#ifndef RHARCHIVE_sysexec_h_INCLUDED
#define RHARCHIVE_sysexec_h_INCLUDED

#include <sys/types.h>
#include "options.h"

struct child_struct {
  pid_t pid_;
  char* script_;
  int err_fd_;
  char** argv_;
  char** evp_;
};
typedef struct child_struct child_t;

child_t* new_child(const char* script, char* const argv[], char* const evp[]);
void free_child(child_t* child);

child_t* rh_exec(const char* script, char* const argv[], char* const evp[]);
int rh_waitpid(child_t* child, int* status);

#endif
