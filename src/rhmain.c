/*
 *  rharchive
 *
 *  The Radio Helsinki Archive Daemon
 *
 *
 *  Copyright (C) 2009-2015 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rharchive.
 *
 *  rharchive is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rharchive is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rharchive. If not, see <http://www.gnu.org/licenses/>.
 *
 *  In addition, as a special exception, the copyright holders hereby
 *  grant permission for non-GPL-compatible GStreamer plugins to be used
 *  and distributed together with GStreamer and rharchive.
 *  This permission goes above and beyond the permissions granted by the
 *  GPL license rharchive is covered by.
 */

#include "rhmain.h"

#include <glib.h>
#include <gst/gst.h>

gboolean rhmain_loop_init(RHMainLoop* loop)
{
  if(!loop)
    return FALSE;

  loop->exit_code = 0;
  loop->gloop = g_main_loop_new(NULL, FALSE);
  if(!loop->gloop)
    return FALSE;

  return TRUE;
}

void rhmain_loop_destroy(RHMainLoop* loop)
{
  if(!loop)
    return;

  gst_object_unref(GST_OBJECT(loop->gloop));
}

gint rhmain_loop_run(RHMainLoop* loop)
{
  if(!loop)
    return -2;

  g_main_loop_run(loop->gloop);
  return g_atomic_int_get(&(loop->exit_code));
}

void rhmain_loop_quit(RHMainLoop* loop, gint exit_code)
{
  if(!loop)
    return;

  g_main_loop_quit(loop->gloop);
  g_atomic_int_set(&(loop->exit_code), exit_code);
}
